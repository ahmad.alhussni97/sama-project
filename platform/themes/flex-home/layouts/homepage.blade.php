{!! Theme::partial('header') !!}

<div id="app">
    <div id="ismain-homes">
        {!! Theme::content() !!}
        
        <div class="container" style="max-width: 1435px;padding: 70px 0px;">
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                    <h2>{{__('Frequently Searched')}} </h2>
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <ul class="list-freq-search">
                                <li class="li-feq-list"><a href="properties?type=rent&location=Dubai">{{__('Properties for rent in')}}{{__('Dubai')}}</a></li>
                                <li class="li-feq-list"><a href="properties?type=rent&location=Abu Dhabi">{{__('Properties for rent in')}}{{__('Abu Dhabi')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=rent&location=Ras Al Khaimah">{{__('Properties for rent in')}}{{__('Ras Al Khaimah')}}  </a></li>
                                <li class="li-feq-list"><a href="properties?type=rent&location=Ajman">{{__('Properties for rent in')}}{{__('Ajman')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=rent&location=Fujairah">{{__('Properties for rent in')}}{{__('Fujairah')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=rent&location=Sharjah">{{__('Properties for rent in')}}{{__('Sharjah')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=rent&location=Umm Al Quwain">{{__('Properties for rent in')}}{{__('Umm Al Quwain')}}  </a></li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <ul class="list-freq-search">
                                <li class="li-feq-list"><a href="properties?type=sale&location=Dubai">{{__('Properties for sale in')}}{{__('Dubai')}}</a></li>
                                <li class="li-feq-list"><a href="properties?type=sale&location=Abu Dhabi">{{__('Properties for sale in')}}{{__('Abu Dhabi')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=sale&location=Ras Al Khaimah">{{__('Properties for sale in')}}{{__('Ras Al Khaimah')}}  </a></li>
                                <li class="li-feq-list"><a href="properties?type=sale&location=Ajman">{{__('Properties for sale in')}}{{__('Ajman')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=sale&location=Fujairah">{{__('Properties for sale in')}}{{__('Fujairah')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=sale&location=Sharjah">{{__('Properties for sale in')}}{{__('Sharjah')}} </a></li>
                                <li class="li-feq-list"><a href="properties?type=sale&location=Umm Al Quwain">{{__('Properties for sale in')}}{{__('Umm Al Quwain')}}  </a></li>
      
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col" style="padding: 51px;">
                    <img src="storage/pexels-pixabay-280222.jpg" style="border-radius: 155px;width: 100%;height: 324px;border: 20px solid white;box-shadow: 0px 0px 5px rgba(33,37,41,0.54);">
                </div>
            </div>
        </div>
    </div>
    
</div>

{!! Theme::partial('footer') !!}

