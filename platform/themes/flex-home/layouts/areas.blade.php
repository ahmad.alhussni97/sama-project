{!! Theme::partial('header-about-us') !!}
<div style="display: block; background: #377cd038;height:105px;"></div>
<div id="app">
    <div id="is-areas">
        <div class="container">
            <div style="position: relative;width: 100%;height: 600px;">
                <a href="properties?type=rent&location=Fujairah" style="position: absolute;z-index: 1;top: 100px;">
                    <img src="/storage/fujairah.png">
                </a>
                <a href="properties?type=sale&location=Abu Dhabi" style="position: absolute;z-index: 0;top: 50px;left: 277px;">
                    <img src="/storage/abu-dhabi.png">
                </a>
                <a href="properties?type=sale&location=Ajman" style="position: absolute;z-index: 2;top: 20px;left: 700px;">
                    <img src="/storage/ajman.png">
                </a>
                <a href="properties?type=rent&location=Dubai" style="position: absolute;z-index: 1;top: 219px;left: 557px;">
                    <img src="/storage/dubai.png">
                </a>
                <a href="properties?type=sale&location=Sharjah" style="position: absolute;z-index: 4;top: 326px;left: 208px;">
                    <img src="/storage/sharjah.png">
                </a>
                <a href="properties?type=sale&location=Umm Al Quwain" style="position: absolute;z-index: 2;top: 297px;left: -38px;">
                    <img src="/storage/um-al-quwain.png">
                </a>
                <a href="properties?type=sale&location=Ras Al Khaimah" style="position: absolute;z-index: 0;top: 219px;left: 750px;">
                    <img src="/storage/ras-al-khaima.png">
                </a>
            </div>
        </div>
        <div class="container container-margin" style="padding-top:60px;">
            <h1 style="color: rgb(0,0,0);">What Can You Buy in UAE?</h1>
            <div class="row">
                <div class="col-auto">
                    <p style="font-size: 284px;font-weight: 200;line-height: 218px;color: #9fc4de;">?</p>
                </div>
                <div class="col">
                    <p>The real question is not “What can you buy in UAE?” but rather “What can you NOT buy in UAE?”. Real estates in UAE are one of the best options when it comes to investing and making the most out of the owned property. The United Arab Emirates is one of the countries that contain a large number of real estate properties that can be bought or rented. Emirates properties can include real estates in Dubai, real estates in Abu Dhabi, real estates in Ajman, and many other options.</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h2>Why Shall You Buy in UAE?</h2>
                </div>
            </div>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">Safety</h1>
            <p style="padding-top: 38px;">The United Arab Emirates is considered to be one of the countries where crime rates are very low and living in it means forever living in one of the safest countries in the whole world. Your property is safe, your future is safe, YOU are safe. This means that you can choose from real estates in Dubai or in other locations. Safety is taking over the whole country.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">Somewhat Cheap</h1>
            <p style="padding-top: 38px;">Unlike what many believe to be true, real estates in UAE are not as expensive as other cosmopolitan hubs. This means that buying in UAE is considered to be cheap and affordable mainly because of the wide selection of properties in the country. Dubai property is one of the options that buyers and investors look for.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">High Rent</h1>
            <p style="padding-top: 38px;">Making the decision of buying real estates in UAE means that owners will benefit from renting out their properties since rent is considered to be high in the United Arab Emirates. This is the concretized definition of wise thinking. As real estate experts, we highly advise our customers to think outside the box; buy and rent out. No one has ever regretted it.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">Great for investment &amp; business purposes</h1>
            <p style="padding-top: 38px;">Real estates in UAE are great for investment and business purposes, especially that it includes cosmopolitan hubs such as Dubai. It also welcomes millions of tourists each year, making it one of the most visited countries in the whole world. Dubai real estate is a very good option to start growing and targeting the right audience in the right country.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">Excellent Social Life</h1>
            <p style="padding-top: 38px;">To buy property in Dubai or anywhere else in the United Arab Emirates, is to benefit from the exceptional social life. The lifestyle UAE is one of a kind, since it is the true definition of luxury. One can have dinner, enjoy a day at the beach, or even spend all day shopping. Nightlife in the United Arab Emirates, especially in Dubai, is one of the best features of the country.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">No Taxes</h1>
            <p style="padding-top: 38px;">The investor-friendly country offers buyers the chance to work, live, and own real estates in UAE without having to pay taxes. Everything you earn is entirely yours to keep. This is another great reason that makes buying real estates in UAE one of the best options. Living tax-free is a dream itself.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">Cheaper Than Rent</h1>
            <p style="padding-top: 38px;">Since rent is considered to be somewhat expensive in the United Arab Emirates, buying real estates in UAE is definitely better than renting a property to live in. Think about all the money you can save on the long run. It’s all about perspective anyway. And good math, obviously. Buy, rent out, live the luxurious life you have always dreamt of.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">Strategic Location</h1>
            <p style="padding-top: 38px;">In the United Arab Emirates, everything falls into place. The strategic location of this country is one of its best features. Thus, all it takes is a 6-hour flight to get to the majority of the destinations. Not to mention the cosmopolitan hubs it includes such as Dubai and Abu Dhabi, making the UAE a once-in-a-lifetime opportunity.</p>
        </div>
        <div class="container" style="margin-top: 76px;margin-bottom: 76px;">
            <h2 class="text-center">Who Can Buy/ Rent in UAE?</h2><img src="storage/whocanbuy-areas.png">
        </div>
        <h2 class="text-center">How Can You Buy/ Rent in UAE?</h2>
        <div class="container text-center" style="margin-top: 81px;margin-bottom: 104px;"><img src="storage/areas-image.jpg"></div>
        <div class="container" style="background: #e3eef6;border-radius: 20px;padding-top: 80px;padding-bottom: 50px;margin-bottom: 84px;">
            <div style="padding-left: 144px;">
                <h2 style="color: #148AC8;font-weight: bolder;font-size: 40px;">Buy/ Rent in UAE Now!</h2>
                <p style="padding-left: 71px;font-size: 18px;padding-top: 40px;padding-right: 142px;">How do you feel being very close to turning your dream into reality? How do you feel about the milestone you are about to reach? We get it, buying real estates in UAE or renting real estates in UAE might be overwhelming, especially if you do not have enough insights, but with the help of professionals, it’s a cinch! And remember, better late than never, but late is never better. So stop dreaming and start doing!</p>
                <p style="padding-left: 71px;font-size: 30px;padding-top: 40px;padding-right: 142px;">Just remember, lights will NOT guide you home, but SAMA UAE definitely will!</p>
            </div>
            <div class="row" style="margin-top: 41px;">
                <div class="col" style="text-align: center;"><a href="#" style="margin: 10px auto;text-align: center;padding:20px;border:#044e8a solid 1px; color:#044e8a;">Sell with us now</a></div>
            </div>
        </div>
        {!! Theme::content() !!}
    </div>
</div>

{!! Theme::partial('footer') !!}

