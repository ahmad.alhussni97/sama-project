{!! Theme::partial('header-about-us') !!}

<div id="app">
    <div id="is-about-us">
        {!! Theme::content() !!}
    </div>
</div>

{!! Theme::partial('footer') !!}

