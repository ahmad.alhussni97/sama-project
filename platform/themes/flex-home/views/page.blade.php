@php
  Theme::set('page', $page);
@endphp

@if (in_array($page->template, ['default', 'full-width']))
    <div class="bgheadproject hidden-xs" style="background: url('{{ theme_option('breadcrumb_background') ? RvMedia::url(theme_option('breadcrumb_background')) : Theme::asset()->url('images/banner-du-an.jpg') }}')">
        <div class="description">
            <div class="container-fluid w90">
                <h1 class="text-center">{{ $page->name }}</h1>
                {!! Theme::partial('breadcrumb') !!}
            </div>
        </div>
    </div>
    <div class="@if ($page->template != 'full-width') container @endif padtop50">
        <div class="row">
            <div class="col-sm-12">
                <div class="scontent">
                    {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
@elseif (in_array($page->template, ['about-us']))
    <div class="bgheadproject hidden-xs about-us-bg" style="background: url('/storage/general/about-us-new-bg.jpg')">
        <div class="description">
            <div class="container-fluid w90">

                {{-- {!! Theme::partial('breadcrumb') !!} --}}
            </div>

        </div>
    </div>
    <div class="container-fluid text-center d-xxl-flex justify-content-xxl-center about-us-sub-menu d-flex align-items-center justify-content-center">
        <ul class="nav align-items-end">
            <li class="nav-item about-us-menu" style="color: #034D8A;"><a class="nav-link" href="#"
                style="color: #034D8A;">{{__('About US')}}</a></li>
        <li class="nav-item about-us-menu"><a class="nav-link" href="#" style="color: #034D8A;">{{__('Our Services')}} </a></li>
        <li class="nav-item about-us-menu"><a class="nav-link" href="#" style="color: #034D8A;">{{__('Marketing and Ads')}} </a></li>
        <li class="nav-item about-us-menu"><a class="nav-link" href="#" style="color: #034D8A;">{{__('Our Team')}} </a></li>
        </ul>
    </div>
    <div class="@if ($page->template != 'full-width') container-fluid @endif padtop50">
        <div class="row">
            <div class="col-sm-12">
                <div class="scontent">
                    {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
@elseif (in_array($page->template, ['blog']))
<div class="bgheadproject hidden-xs blog-bg" style="background: url('/storage/general/about-us-new-bg.jpg')">
    <div class="description">
        <div class="container-fluid w90">

            {{-- {!! Theme::partial('breadcrumb') !!} --}}
        </div>

    </div>
</div>

<div class="@if ($page->template != 'full-width') container @endif padtop50">
    <div class="row">
        <div class="col-sm-12">
            <div class="scontent">
                {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
            </div>
        </div>
    </div>
</div>
<br>
<br>
@else
    {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
@endif
