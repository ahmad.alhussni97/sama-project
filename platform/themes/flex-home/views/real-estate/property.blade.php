@php
    Theme::asset()->usePath()->add('leaflet-css', 'libraries/leaflet.css');
    Theme::asset()->container('footer')->usePath()->add('leaflet-js', 'libraries/leaflet.js');
    Theme::asset()->usePath()->add('magnific-css', 'libraries/magnific/magnific-popup.css');
    Theme::asset()->container('footer')->usePath()->add('magnific-js', 'libraries/magnific/jquery.magnific-popup.min.js');
    Theme::asset()->container('footer')->usePath()->add('property-js', 'js/property.js');
@endphp
<main class="detailproject bg-white">
    <div data-property-id="{{ $property->id }}"></div>

    {{-- header image goes here --}}
    <div style="width: 100%;"><img style="width: 100%;height:500px;object-fit: cover;border-radius: 40px;" src="{{ RvMedia::getImageUrl($property->images[1], null, false, RvMedia::getDefaultImage()) }}" alt=""></div>
    <div class="container-fluid" style="width: 80%;">
        <div class="row">
            <div class="col" style="padding: 44px;">
                <div class="row">
                    <div class="col" style="border-left: 4px solid #ced8e3 ;">
                        <h1 style="color: rgb(3,77,138);font-size: 32px;font-weight: 300;/*border-left: 3px solid #ced8e3;*/padding-left: 12px;padding-bottom: 0px;">{{ $property->name }}</h1>
                    </div>
                </div>
                <div class="row" style="border-bottom: 4px solid #ced8e3;border-left: 4px solid #ced8e3;border-bottom-left-radius: 20px;margin-top: 23px;">
                    <div class="col" style="padding-bottom: 8px;"><span style="color: rgb(110,116,120);font-weight: 600;font-size: 34px;padding-left: 12px;">{{ $property->price_html }}</span></div>
                </div>
                <div class="row">
                    <div class="col">
                        @php $first_word_city_name = explode(',', trim($property->city_name)); @endphp
                        <h2 style="font-size: 28px;padding: 18px;">{{__('Location')}} </h2><span style="padding: 18px;">{{$property->location}}, {{ $first_word_city_name[0] ??  $property->city_name}}</span>
                    </div>
                </div>
            </div>
            <div class="col-1" style="padding-top: 44px;"><i class="fa fa-heart-o" style="color: rgb(112,118,122);font-size: 35px;"></i></div>
            {{-- property agency box --}}
            @if ($author = $property->author)
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5 col-xxl-5" style="position: relative;top: -75px;z-index: 9;/*background: #e1e6ec;*/border-radius: 20px;/*height: 300px;*/">
                <div class="row" style="height: 294px;background: #e1e6ec;/*height: 300px;*/border-top-left-radius: 20px;border-top-right-radius: 20px;border-top-left-radius: 20px;">
                    <div class="col" style="padding: 0px;z-index: 9;height: 100%;">

                        <div>
                            @if ($author->username)
                            <a href="{{ route('public.agent', $author->username) }}" style="position: relative;top: 80px;width: 80%;height: 155px;margin: 0 auto;background: #f0f7ff;border-radius: 20px;box-shadow: 0px 0px 4px rgba(99,99,99,0.53);display: flex;">
                                @if ($author->avatar->url)
                                    <img src="{{ RvMedia::getImageUrl($author->avatar->url, 'thumb') }}" alt="{{ $author->name }}" class="img-thumbnail">
                                @else
                                    <img src="/storage/layer1.svg" alt="{{ $author->name }}" class="img-thumbnail">
                                @endif
                            </a>
                            @endif
                        </div>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col" style="padding-top: 83px;padding-left: 12px;">
                                <h1 style="font-size: 30px;">
                                    @if ($author->username)
                                    <a href="{{ route('public.agent', $author->username) }}">{{__('SAMA UAE')}} - {{ $author->name }}</a>
                                    @else
                                        {{ $author->name }}
                                    @endif
                                </h1>
                                <p style="margin-bottom: 37px;">
                                    @php
                                        if ($author->phone) {
                                           Theme::set('hotlineNumber', $author->phone);
                                        }
                                    @endphp
                                    <span class="mobile">{{ $author->phone ?: theme_option('hotline') }}</span>
                                    <br>{{ $author->email }}
                                    <br>@if($author->is_verified == 0 ){{__('Not verified')}} @else {{__('Verified')}} @endif </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="background: #034d8a;border-bottom-right-radius: 20px;border-bottom-left-radius: 20px;">
                    <div class="col" style="text-align: center;padding: 20px;"><a href="#" style="color: rgb(255,255,255);">
                        @if ($author->username)
                        <a href="{{ route('public.agent', $author->username) }}" style="color: #fff;">{{ __('View Full Profile') }}</a>
                        @endif
                    </a></div>
                </div>
                <div class="row" style="position: relative;top: 27px;">
                    <div class="col">
                        <div class="row">
                            <div class="col-6" style="padding:0 2px !important;">
                                <div class="social-icons social-icons-blue" style="padding: 9px 10px !important;background: rgb(225,230,236);border-radius: 5px;"><span style="color: #2c3949;">{{__('Share this listing')}} </span><a href="http://www.twitter.com/share?url={{Request::fullUrl()}}"><i class="fab fa-twitter" style="color: #148ac8;"></i></a><a href="https://www.facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}"><i class="fab fa-facebook-square" style="color: #148ac8;"></i></a><a href="https://www.facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}"><i class="fab fa-instagram" style="color: #148ac8;"></i></a></div>
                            </div>

                            <div class="col-3" style="padding:0 2px !important;"><a href="tel:{{$property->phone ?? ''}}" class="btn btn-outline-link" type="button" style="border: 2px solid #b1c6dd;padding: 10px 12px;width: 100%;">{{__('Call Us')}} </a></div>

                            <div class="col-3" style="padding:0 2px !important;"><a href="mailto:{{$property->email ?? ''}}" class="btn btn-outline-link" type="button" style="border: 2px solid #b1c6dd;padding: 10px 12px;width: 100%;">{{__('Send Inquiry')}}</a></div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="container-fluid w90 padtop20" style="width:80%">
        {{-- <h1 class="titlehouse">{{ $property->name }}</h1>
        <p class="addresshouse">
            <i class="fas fa-map-marker-alt"></i> {{ $property->city_name }}
            @if (setting('real_estate_display_views_count_in_detail_page', 0) == 1)
                <span class="d-inline-block" style="margin-left: 10px"><i class="fa fa-eye"></i> {{ $property->views }} {{ __('views') }}</span>
            @endif
            <span class="d-inline-block" style="margin-left: 10px"><i class="fa fa-calendar-alt"></i> {{ $property->created_at->translatedFormat('M d, Y') }}</span>
        </p>
        <p class="pricehouse"> {{ $property->price_html }} {!! $property->status_html !!}</p> --}}
        <div class="row" >
            <div class="col">
                <div class="row pt-3" >
                    <div class="col-sm-12">
                        <h5 class="headifhouse">{{ __('Building Details Overview') }}</h5>

                        <div class="row">
                            <div class="col">
                                <ul class="list-unstyled" style="line-height: 41px;">
                                    <li><span class="propertie-details-title">{{__('Reference No.')}}:<br></span>&nbsp;{{$property->refrance_no}}<br></li>
                                    <li><span class="propertie-details-title">{{__('Agent')}}:<br></span>&nbsp;{{$property->author->name}}<br></li>
                                    <li><span class="propertie-details-title">{{__('Property Purpose')}}:<br></span>&nbsp;{{$property->type}}<br></li>
                                    @if ($property->categories()->count())
                                    <li><span class="propertie-details-title">{{__('Property Type')}}:<br></span>&nbsp;{{ implode(', ', array_unique($property->categories()->pluck('name')->all())) }}<br></li>
                                    @endif
                                    <li><span class="propertie-details-title">{{__('Property Status')}}:<br></span>&nbsp;
                                        @if($property->property_status == 1){{__('Off-plan')}} @elseif ($property->property_status ==2){{__('Launching Soon')}}  @elseif ($property->property_status ==3){{__('Under Construction')}}  @else{{__('Completed')}}  @endif

                                        <br></li>
                                    @if ($property->square)
                                    <li><span class="propertie-details-title">{{__('Property Size')}}:<br></span>&nbsp;{{ $property->square_text }}<br></li>
                                    @endif

                                    <li><span class="propertie-details-title">{{__('Listing Date')}}:<br></span>&nbsp;{{ $property->created_at->translatedFormat('M d, Y') }}<br></li>
                                    {{-- <li><span class="propertie-details-title">{{__('Property Type')}}:<br></span>&nbsp;{{$property->Category->name}}<br></li> --}}

                                    <li><span class="propertie-details-title">{{__('Furnishing Status')}}:</span>
                                    @if($property->is_furnished == 1){{__('Furnished')}}  @elseif ($property->is_furnished ==2){{__('Not furnished')}}  @else {{__('Partly furnished')}}  @endif
                                    </li>
                                </ul>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <ul class="list-unstyled" style="line-height: 41px;">
                                            @if ($property->number_bedroom)
                                            <li><i class="fas fa-bed propertie-details-icon"></i><span style="padding-left: 10px;font-size: 19px;">{{ __('BEDS') }} ( {{ number_format($property->number_bedroom) }})</span></li>
                                            @endif
                                            @if ($property->number_bathroom)
                                            <li><i class="fas fa-shower propertie-details-icon"></i><span style="padding-left: 10px;font-size: 19px;">{{ __('BATH') }} ( {{ number_format($property->number_bathroom)}} )</span></li>
                                            @endif
                                            @if ($property->number_floor)
                                            <li><i class="fas fa-bed propertie-details-icon" style="font-size: 28px;"></i><span style="padding-left: 10px;font-size: 19px;">{{ __('floors') }}( {{ number_format($property->number_floor) }} )</span></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                @if ($property->features->count())
                                <div class="row">
                                    <div class="col">
                                        <h2 style="color: #034D8A;font-size: 34px;font-weight: 300;padding-bottom: 26px;">{{ __('Amenities') }}</h2>
                                        @php $property->features->loadMissing('metadata'); @endphp
                                        <div>
                                            @foreach($property->features as $feature)
                                            <span class="amenities"> {{ $feature->name }}</span>
                                            @endforeach
                                          </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                @if ($property->content)
                    <div class="row" style="background: #dbe1e8;border-radius: 5px;padding: 34px;">
                        <div class="col-sm-5">
                            <h5 class="headifhouse" style="border-top: none;margin-top: 0px !important;">{{ __('Property Description') }}</h5>
                            <div class="content-property">
                                {!! BaseHelper::clean($property->content) !!}
                            </div>

                        </div>
                        <div class="col-sm-7">

                            @include(Theme::getThemeNamespace() . '::views.real-estate.includes.slider', ['object' => $property])
                        </div>

                    </div>
                @endif

                <br>
                @if ($property->facilities->count())
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="headifhouse">{{ __('Services and Facilities') }}</h5>
                            <div class="row">
                                @php $property->facilities->loadMissing('metadata'); @endphp
                                @foreach($property->facilities as $facility)
                                    <div class="col-sm-4">
                                        @if ($facility->getMetaData('icon_image', true))
                                            <p><i><img src="{{ RvMedia::getImageUrl($facility->getMetaData('icon_image', true)) }}" alt="{{ $facility->name }}" style="vertical-align: top; margin-top: 3px;" width="18" height="18"></i> {{ $facility->name }} - {{ $facility->pivot->distance }}</p>
                                        @else
                                            <p><i class="@if ($facility->icon) {{ $facility->icon }} @else fas fa-check @endif text-orange text0i"></i> {{ $facility->name }} - {{ $facility->pivot->distance }}</p>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                @if ($property->project_id && $project = $property->project)
                    <div class="row pb-3">
                        <div class="col-sm-12">
                            <h5 class="headifhouse">{{ __("Project's information") }}</h5>
                        </div>
                        <div class="col-sm-12">
                            <div class="row item">
                                <div class="col-md-4 col-sm-5 pr-sm-0">
                                    <div class="img h-100 bg-light">
                                        <a href="{{ $project->url }}">
                                            <img class="thumb lazy"
                                                data-src="{{ RvMedia::getImageUrl($project->image, null, false, RvMedia::getDefaultImage()) }}"
                                                src="{{ RvMedia::getImageUrl($project->image, null, false, RvMedia::getDefaultImage()) }}"
                                                alt="{{ $project->name }}">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7 pt-2 pr-sm-0 bg-light">
                                    <h5><a href="{{ $project->url }}" class="font-weight-bold text-dark">{{ $project->name }}</a></h5>
                                    <div>{{ Str::limit($project->description, 120) }}</div>
                                    <p><a href="{{ $project->url }}">{{ __('Read more') }}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <br>
                @if ($property->latitude && $property->longitude)
                    {!! Theme::partial('real-estate.elements.traffic-map-modal', ['location' => $property->location . ', ' . $first_word_city_name[0],'phone'=>$property->phone]) !!}
                @else
                    {!! Theme::partial('real-estate.elements.gmap-canvas', ['location' => $property->location]) !!}
                @endif
                {{-- @if ($property->video_url)
                    {!! Theme::partial('real-estate.elements.video', ['object' => $property, 'title' => __('Property video')]) !!}
                @endif --}}
                <br>
                {!! Theme::partial('share', ['title' => __('Share this property'), 'description' => $property->description]) !!}
                <div class="clearfix"></div>
            </div>

        </div>
        <br>
        <h5 class="headifhouse">{{ __('Properties in Nearby Area') }}</h5>

        <div class="projecthome mb-3">
            <property-component type="related" url="{{ route('public.ajax.properties') }}" :property_id="{{ $property->id }}"></property-component>
        </div>
    </div>
</main>

<script id="traffic-popup-map-template" type="text/x-custom-template">
    {!! Theme::partial('real-estate.properties.map', ['property' => $property]) !!}
</script>
