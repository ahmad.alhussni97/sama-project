@php
    if (theme_option('show_map_on_properties_page', 'yes') == 'yes') {
        Theme::asset()->usePath()->add('leaflet-css', 'libraries/leaflet.css');
        Theme::asset()->container('footer')->usePath()->add('leaflet-js', 'libraries/leaflet.js');
        Theme::asset()->container('footer')->usePath()->add('leaflet.markercluster-src-js', 'libraries/leaflet.markercluster-src.js');
    }
@endphp

<section class="main-homes pb-3 properties-page">
    <div class="bgheadproject hidden-xs about-us-bg" style="background: url('/storage/general/mask-group-39-min.jpg')">
        <div class="description">
            <h1 class="text-center">{{ SeoHelper::getTitle() }}</h1>
            <p class="text-center">{{ theme_option('properties_description') }}</p>
            {!! Theme::partial('breadcrumb') !!}
            <dev class="list_menu">
                <span>breadCrumps</span> > <span>breadCrumps</span> > <span
                    class="list_menu_active">breadCrumps</span>
            </dev>
        </div>
        <div class="container container_project">
            <div class="panel panel-default">
                <div class="panel-body panel-sell-with-us">Sell With <b>Us</b></div>
            </div>
        </div>
    </div>
</section>

@if (theme_option('show_map_on_properties_page', 'yes') == 'yes')
    <script id="traffic-popup-map-template" type="text/x-custom-template">
        {!! Theme::partial('real-estate.properties.map', ['property' => get_object_property_map()]) !!}
    </script>
@endif

<section>
    <div class="big-title-sell-with-us">
        <p class="color_black font_weight_inherit">Fill out the form and our specialists will contact you!</p>
    </div>
    <div class="container">
        <form id="form-sell-with-us">
            <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline1">Mr.</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline2">Mrs.</label>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationDefault01">First name*</label>
                    <input type="text" class="form-control sell-input" id="validationDefault01" placeholder="First name"
                           value="Mark" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationDefault02">Last name*</label>
                    <input type="text" class="form-control sell-input" id="validationDefault02" placeholder="Last name"
                           value="Otto" required>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationDefault03">E-Mail*</label>
                    <input type="text" class="form-control sell-input" id="validationDefault03" placeholder="City"
                           required>
                </div>
                <div class="col-md-1 mb-3">
                    <label for="validationDefault04">Phone</label>
                    <input type="text" class="form-control sell-input" id="validationDefault04" placeholder="State"
                           required>
                </div>
                <div class="col-md-5 phone-input">
                    <input type="text" class="form-control sell-input" id="validationDefault04" placeholder="State"
                           required>
                </div>

            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationDefault01">Country</label>
                    <select name="type" id="select-type" class="form-control sell-input">
                        <option value="">{{ __('-- Select --') }}</option>
                        <option value="">1</option>
                        <option value="">2</option>
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationDefault01">Property Type</label>
                    <select name="type" id="select-type" class="form-control sell-input">
                        <option value="">{{ __('-- Select --') }}</option>
                        <option value="">1</option>
                        <option value="">2</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <label for="validationDefault01">Message</label>
                    <textarea name="content" class="form-control sell-input" rows="5"
                              placeholder="{{ __('Message') }} *"></textarea>
                </div>
            </div>
            <div class="form-row mt-4">
                <div class="col-md-10">
                    <input type="checkbox" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">I agree ti the <u>term of service and privacy
                            policy</u></label>
                </div>

                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-block btn-submit-form">Submit Form</button>
                </div>
            </div>

        </form>
    </div>
</section>

<section>
    <div class="container container-sell-content">
        <div class="sell-content">
            <h2 class="text-left color_basic">What is SAMA??</h2>
            <div class="container">
                <p class="color_black sell-description">The United Arab Emirates is considered to be one of the
                    countries where crime rates are very low and living in it means forever living in one of the safest
                    countries in the whole world. Your property is safe, your future is safe, YOU are safe. This means
                    that you can choose from real estates in Dubai or in other locations. Safety is taking over the
                    whole country.</p>
            </div>
        </div>
        <div class="sell-content">
            <h2 class="text-left color_basic">What Can You Sell on SAMA?</h2>
            <div class="container">
                <p class="color_black sell-description">The United Arab Emirates is considered to be one of the
                    countries where crime rates are very low and living in it means forever living in one of the safest
                    countries in the whole world. Your property is safe, your future is safe, YOU are safe. This means
                    that you can choose from real estates in Dubai or in other locations. Safety is taking over the
                    whole country.</p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container padtop40">
        <div class="row mt-5">
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp" class="img-circle"
                     alt="Cinque Terre">
                <div class="centered-place-text-circle">Text</div>
            </div>
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                     class="img-thumbnail" alt="Cinque Terre">
                <div class="centered-place-text">Text</div>
            </div>
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp" class="img-circle"
                     alt="Cinque Terre">
                <div class="centered-place-text-circle">Text</div>
            </div>
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                     class="img-thumbnail" alt="Cinque Terre">
                <div class="centered-place-text">Text</div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp" class="img-circle"
                     alt="Cinque Terre">
                <div class="centered-place-text-circle">Text</div>
            </div>
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                     class="img-thumbnail" alt="Cinque Terre">
                <div class="centered-place-text">Text</div>
            </div>
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp" class="img-circle"
                     alt="Cinque Terre">
                <div class="centered-place-text-circle">Text</div>
            </div>
            <div class="col-md-3">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                     class="img-thumbnail" alt="Cinque Terre">
                <div class="centered-place-text">Text</div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid  padding-mask-group">
        <div class="row">
            <div class="col"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: center;"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: right;"><img src="/storage/mask-group-33.png"></div>
        </div>
    </div>
</section>

<section>
    <div class="container padtop10">
        <div class="row">
            <div class="about-us-wrap-custom">
                <div class="container">
                    <h1>Why Should You Trust SAMA ?</h1>
                </div>
                <div class="sell-content-2">
                    <h1 class="color_basic_2 font-bolder">Knowledge&amp; Expertise</h1>
                    <div class="container">
                        <p class="color_black sell-description">The United Arab Emirates is considered to be one of the
                            countries where crime rates are very low and living in it means forever living in one of the
                            safest
                            countries in the whole world. Your property is safe, your future is safe, YOU are safe. This
                            means
                            that you can choose from real estates in Dubai or in other locations. Safety is taking over
                            the
                            whole country.</p>
                    </div>
                </div>

                <div class="sell-content-2">
                    <h1 class="color_basic_2 font-bolder">Transparency</h1>
                    <div class="container">
                        <p class="color_black sell-description">The United Arab Emirates is considered to be one of the
                            countries where crime rates are very low and living in it means forever living in one of the
                            safest
                            countries in the whole world. Your property is safe, your future is safe, YOU are safe. This
                            means
                            that you can choose from real estates in Dubai or in other locations. Safety is taking over
                            the
                            whole country.</p>
                    </div>
                </div>

                <div class="sell-content-2">
                    <h1 class="color_basic_2 font-bolder">Commitment</h1>
                    <div class="container">
                        <p class="color_black sell-description">The United Arab Emirates is considered to be one of the
                            countries where crime rates are very low and living in it means forever living in one of the
                            safest
                            countries in the whole world. Your property is safe, your future is safe, YOU are safe. This
                            means
                            that you can choose from real estates in Dubai or in other locations. Safety is taking over
                            the
                            whole country.</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>


<section>
    <div class="sell-marketing">
        <div class="marketing-ads-section">
            <h1 style="color: rgb(255, 255, 255); text-align: center;">Marketing and Ads</h1>
            <section class="container marketing-text-box">
                <div class="row" style="background-color: rgb(255, 255, 255);">
                    <div class="col"><p class="color_black font_size_20 mb-5">Obviously, in a world full of new
                            trends and with technology taking over our minds, SAMA UAE offers its
                            clients marketing services and ads to be able to attract potential buyers,
                            renters, or investors.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>That
                            being said, we handle the task of marketing your property in the best way to
                            get the best offer. That is done by our professionals and social media
                            experts who know the key factors that must be highlighted. Our experts do
                            their utmost to offer clients the best marketing strategies.</p>
                        <p><a href="#" class="btn-markting">Sell with us now!</a></p>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<section>

    <div class="text-center color_basic_2 consultancy-q">
        <h1>Why Should You Trust SAMA ?</h1>
    </div>

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item carousel-item-2 active">
                <div class="slide-sell-us d-block w-100 h-100">
                    <div class="container">
                        <div class="row">
                            <div class="about-us-wrap-custom padding-top-8">
                                <h1 class="color_white font-bolder">Consultancy</h1>
                                <div class="sell-content-2">
                                    <h4 class="font-bolder rgb-135-181-214">Knowledge&amp; Expertise</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">
                                            The United Arab Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p></div>
                                </div>
                                <div class="sell-content-2"><h4 class="rgb-135-181-214 font-bolder">Transparency</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">The United Arab
                                            Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p></div>
                                </div>
                                <div class="sell-content-2"><h4 class="rgb-135-181-214 font-bolder">Commitment</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">The United Arab
                                            Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item carousel-item-2">
                <div class="slide-sell-us d-block w-100 h-100">
                    <div class="container">
                        <div class="row">
                            <div class="about-us-wrap-custom padding-top-8">
                                <h1 class="color_white font-bolder">Consultancy</h1>
                                <div class="sell-content-2">
                                    <h4 class="font-bolder rgb-135-181-214">Knowledge&amp; Expertise</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">
                                            The United Arab Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p></div>
                                </div>
                                <div class="sell-content-2"><h4 class="rgb-135-181-214 font-bolder">Transparency</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">The United Arab
                                            Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p></div>
                                </div>
                                <div class="sell-content-2"><h4 class="rgb-135-181-214 font-bolder">Commitment</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">The United Arab
                                            Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item carousel-item-2">
                <div class="slide-sell-us d-block w-100 h-100">
                    <div class="container">
                        <div class="row">
                            <div class="about-us-wrap-custom padding-top-8">
                                <h1 class="color_white font-bolder">Consultancy</h1>
                                <div class="sell-content-2">
                                    <h4 class="font-bolder rgb-135-181-214">Knowledge&amp; Expertise</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">
                                            The United Arab Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p></div>
                                </div>
                                <div class="sell-content-2"><h4 class="rgb-135-181-214 font-bolder">Transparency</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">The United Arab
                                            Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p></div>
                                </div>
                                <div class="sell-content-2"><h4 class="rgb-135-181-214 font-bolder">Commitment</h4>
                                    <div class="container">
                                        <p class="color_white sell-description">The United Arab
                                            Emirates is considered to be one of the
                                            countries where crime rates are very low and living in it means forever
                                            living in one of the
                                            safest
                                            countries in the whole world. Your property is safe, your future is safe,
                                            YOU are safe. This
                                            means
                                            that you can choose from real estates in Dubai or in other locations. Safety
                                            is taking over
                                            the
                                            whole country.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="position:relative;bottom: 15px;">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleControls"
                    data-slide-to="0" class="active circle-slide"></li>
                <li data-target="#carouselExampleControls"
                    data-slide-to="1" class="circle-slide"></li>
                <li data-target="#carouselExampleControls"
                    data-slide-to="2" class="circle-slide"></li>
            </ol>
            <div>
                <a class="carousel-control-prev carousel-control-prev-2" href="#carouselExampleControls" role="button"
                   data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next carousel-control-next-2" href="#carouselExampleControls" role="button"
                   data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

    </div>
</section>

<section>
    <div class="text-center color_basic_2 padding-mask-number">
        <h1>Why Should You Trust SAMA ?</h1>
    </div>
    <div class="container text-center about-us-section about-us-section-custom" style=" width: 1400px;">
                <p>
                    <img src="/storage/general/3_1.svg" alt="Group%204015%20(1).svg" class="line-1-custom">
                    <img src="/storage/general/1_1.svg" alt="" class="line-2-custom">
                    <img src="/storage/general/2_2.svg" alt="" class="line-3-custom">
                </p>
        <div class="row padding-10">
            <div class=""><img class="number-icon" src="/storage/general/1.svg" alt="about1.png"></div>
            <div class="col">
                <p style="font-size: 18px;text-align: left;" class="color_basic_2">
                    Facility
                    management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Rental management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Property management</p></div>
        </div>
        <div class="row padding-10">
            <div class="col" style="text-align: right;">
                <img class="number-icon" src="/storage/general/2.svg" alt="about1.png"></div>
            <div class="col">
                <p style="font-size: 18px;text-align: left;" class="color_basic_2">
                    Negotiations&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Property showcase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Listings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Process of selling, buying, renting, and renting out a property</p></div>

        </div>
        <div class="row padding-10">
            <div class=""><img class="number-icon" src="/storage/general/3.svg" alt="about1.png"></div>
            <div class="col">
                <p style="font-size: 18px;text-align: left;" class="color_basic_2">
                    Facility
                    management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Rental management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Property management</p></div>
        </div>
        <div class="row padding-10">
            <div class="col" style="text-align: right;">
                <img class="number-icon" src="/storage/general/4.svg" alt="about1.png"></div>
            <div class="col">
                <p style="font-size: 18px;text-align: left;" class="color_basic_2">
                    Negotiations&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Property showcase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Listings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                    Process of selling, buying, renting, and renting out a property</p></div>

        </div>

    </div>
</section>


<section>
    <div class="container-fluid padding-mask-group" style=";padding-right: 0;padding-left: 0;">
        <div class="row">
            <div class="col"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: center;"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: right;"><img src="/storage/mask-group-33.png"></div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid blog">

        <div>
            <p class="blog-title">Our Blog</p>
        </div>

        <!-- Grid row -->

        <!-- Grid row -->
        <div id="gallery">
            <!-- Grid column -->
            <div class="pics animation all 1 custom-col-6">
                <div class="circle-date color_black">
                    <label><span class="font-size-16">28 </span><br>AUG</label>
                </div>
                <img class="img-fluid img-fluid-big" src="https://mdbootstrap.com/img/Photos/Vertical/mountain1.webp"
                     alt="Card image cap">
                <div class="centered">Same group Sets Real Estate World Record , Same group <br>
                    <button type="button" class="btn btn-read-more mt-2 read-more-big-project"> Read More</button>
                </div>
            </div>
            <!-- Grid column -->

            <div class="custom-col-6">
                <div>
                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record <br>
                            <button type="button" class="btn btn-read-more mt-2 read-more-small-project"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record<br>
                            <button type="button" class="btn btn-read-more read-more-small-project mt-2"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record <br>
                            <button type="button" class="btn btn-read-more read-more-small-project mt-2"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record<br>
                            <button type="button" class="btn btn-read-more read-more-small-project mt-2"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->
                </div>
            </div>
        </div>

    </div>
</section>

<section>
    <div class="container" style="max-width: 1435px;margin-left: 41px;margin-right: auto;">
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <h2>{{__('Frequently Searched')}} </h2>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <ul class="list-freq-search">
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Dubai">{{__('Properties for rent in')}}{{__('Dubai')}}</a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Abu Dhabi">{{__('Properties for rent in')}}{{__('Abu Dhabi')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Ras Al Khaimah">{{__('Properties for rent in')}}{{__('Ras Al Khaimah')}}  </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Ajman">{{__('Properties for rent in')}}{{__('Ajman')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Fujairah">{{__('Properties for rent in')}}{{__('Fujairah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Sharjah">{{__('Properties for rent in')}}{{__('Sharjah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Umm Al Quwain">{{__('Properties for rent in')}}{{__('Umm Al Quwain')}}  </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <ul class="list-freq-search">
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Dubai">{{__('Properties for sale in')}}{{__('Dubai')}}</a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Abu Dhabi">{{__('Properties for sale in')}}{{__('Abu Dhabi')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Ras Al Khaimah">{{__('Properties for sale in')}}{{__('Ras Al Khaimah')}}  </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Ajman">{{__('Properties for sale in')}}{{__('Ajman')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Fujairah">{{__('Properties for sale in')}}{{__('Fujairah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Sharjah">{{__('Properties for sale in')}}{{__('Sharjah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Umm Al Quwain">{{__('Properties for sale in')}}{{__('Umm Al Quwain')}}  </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col" style="padding: 51px;">

                <img src="/storage/pexels-pixabay-280222.jpg"
                     style="border-radius: 155px;width: 100%;height: 324px;border: 20px solid white;box-shadow: 0px 0px 5px rgba(33,37,41,0.54);">
            </div>
        </div>
    </div>
</section>

<script>
    const carousel = new bootstrap.Carousel('#myCarousel')
</script>
