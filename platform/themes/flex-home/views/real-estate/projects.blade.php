<section class="main-homes" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <div class="bgheadproject hidden-xs project-bg">
        <div class="description">
            <h1 class="text-center">{{ SeoHelper::getTitle() }}</h1>
            <p class="text-center">{{ theme_option('properties_description') }}</p>
            {!! Theme::partial('breadcrumb') !!}
            <dev class="list_menu">
                <span>breadCrumps</span> > <span>breadCrumps</span> > <span
                    class="list_menu_active">breadCrumps</span>
            </dev>
        </div>
        <div class="container container_project">
            <div class="panel panel-default">
                <div class="panel-body panel-body-project">Our Featured <b>Project</b></div>
            </div>
        </div>

        <div class="container project-slide">
            <div>

                <!--Carousel Wrapper-->
                <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                        <!--First slide-->
                        <div class="carousel-item active">

                            <div class="row row-custom">
                                <div class="col-md-4 col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="carousel-item">

                            <div class="row row-custom">
                                <div class="col-md-4 col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="carousel-item">

                            <div class="row row-custom">
                                <div class="col-md-4 col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(34).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(18).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide">
                                    <div class="mb-2">
                                        <img class="card-img-top"
                                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).jpg"
                                             alt="Card image cap">
                                        <div class="centered-slide">
                                            <div class="project-slide-title"> <span class="font-size-14">Abu Dubai , UAE</span> <br> Same group Sets Real</div>
                                            <button class="button-project-slide">View Project <i class='fas fa-arrow-right'></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="carousel-arrow">
                        <a class="btn-floating  arrow-slide-outline" href="#multi-item-example" data-slide="next">
                            <i class="fas fa-arrow-right arrow-slide"></i>
                            <span class="hide">Next</span>
                        </a>
                    </div>

                    <ol class="carousel-indicators">
                        <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                        <li data-target="#multi-item-example" data-slide-to="1"></li>
                        <li data-target="#multi-item-example" data-slide-to="2"></li>
                    </ol>
                    <!--/.Indicators-->
                </div>
                <!--/.Carousel Wrapper-->


            </div>
        </div>

    </div>


    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
</section>


<section>
    <div class="big-title-projects">
        <p><span class="color_black font_weight_inherit">All </span> <span class="project-title">Projects</span></p>
    </div>
    <div class="list-project">
        <div class="row justify-content-center">
            <div class="col-md-2">
                <select class="form-control border-radius">
                    <option value="">Default Sort</option>
                    <option value="Dubai">Dubai</option>
                    <option value="Abu Dhabi">Abu Dhabi</option>
                    <option value="Sharjah">Sharjah</option>
                    <option value="Ajman">Ajman</option>
                    <option value="Umm Al Quwain">Umm Al Quwain</option>
                    <option value="Fujairah">Fujairah</option>
                    <option value="Ras Al Khaimah">Ras Al Khaimah</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control border-radius">
                    <option value="">Country</option>
                    <option value="Dubai">Dubai</option>
                    <option value="Abu Dhabi">Abu Dhabi</option>
                    <option value="Sharjah">Sharjah</option>
                    <option value="Ajman">Ajman</option>
                    <option value="Umm Al Quwain">Umm Al Quwain</option>
                    <option value="Fujairah">Fujairah</option>
                    <option value="Ras Al Khaimah">Ras Al Khaimah</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control border-radius">
                    <option value="">Project type</option>
                    <option value="Dubai">Dubai</option>
                    <option value="Abu Dhabi">Abu Dhabi</option>
                    <option value="Sharjah">Sharjah</option>
                    <option value="Ajman">Ajman</option>
                    <option value="Umm Al Quwain">Umm Al Quwain</option>
                    <option value="Fujairah">Fujairah</option>
                    <option value="Ras Al Khaimah">Ras Al Khaimah</option>
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control border-radius">
                    <option value="">Bedrooms</option>
                    <option value="Dubai">Dubai</option>
                    <option value="Abu Dhabi">Abu Dhabi</option>
                    <option value="Sharjah">Sharjah</option>
                    <option value="Ajman">Ajman</option>
                    <option value="Umm Al Quwain">Umm Al Quwain</option>
                    <option value="Fujairah">Fujairah</option>
                    <option value="Ras Al Khaimah">Ras Al Khaimah</option>
                </select>
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary filter-background" type="button">
                    <i class="fa fa-sliders"></i>
                    Filter
                </button>
            </div>
        </div>
    </div>

    <div class="card-outline">
        <div class="card mb-3 main-card-project">
            <div class="row no-gutters">
                <div class="col-md-3">
                    <div>
                        <img src="/themes/flex-home/images/rectangle-4.png" class="card-img" alt="...">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card-body row">
                        <div class="card-title col-md-8">
                            <p><b class="color_black">City . Status </b></p>
                            <p class="card-text"><span class="color_black">Title</span>
                                <br><span style="opacity: 0.8" class="color_black">Units</span> <br><span>Price per SQ. FT</span>
                            </p>
                        </div>

                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="background-project-icon">
                                           <span class="icon-project">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                        <span class="icon-project">
                                        <i class="fas fa-phone"></i>
                                    </span>
                                        <span class="icon-project">
                                        <i class="fas fa-share-alt"></i>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button class="button-project">View Project <i class='fas fa-arrow-right'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-outline">
        <div class="card mb-3 main-card-project">
            <div class="row no-gutters">
                <div class="col-md-3">
                    <div>
                        <img src="/themes/flex-home/images/rectangle-4.png" class="card-img" alt="...">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card-body row">
                        <div class="card-title col-md-8">
                            <p><b class="color_black">City . Status </b></p>
                            <p class="card-text"><span class="color_black">Title</span>
                                <br><span style="opacity: 0.8" class="color_black">Units</span> <br><span>Price per SQ. FT</span>
                            </p>
                        </div>

                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="background-project-icon">
                                           <span class="icon-project">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                        <span class="icon-project">
                                        <i class="fas fa-phone"></i>
                                    </span>
                                        <span class="icon-project">
                                        <i class="fas fa-share-alt"></i>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button class="button-project">View Project <i class='fas fa-arrow-right'></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="pagination col-md-4 paginate_project">
        <a href="#"><i class='fas fa-angle-double-left font-pagination'></i></a>
        <a href="#"><i class='fas fa-chevron-left font-pagination'></i></a>
        <a href="#">1</a> <label class="between-pagination">|</label>
        <a href="#">2</a> <label class="between-pagination">|</label>
        <a href="#">3</a> <label class="between-pagination">|</label>
        <a href="#">4</a> <label class="between-pagination">|</label>
        <a href="#">5</a>
        <a href="#"><i class='fas fa-chevron-right font-pagination'></i></a>
        <a href="#"><i class=' fas fa-angle-double-right font-pagination'></i></a>
    </div>


</section>

<section>
    <div class="container-fluid" style="margin-top: 52px;margin-bottom: 94px;padding-right: 0;padding-left: 0;">
        <div class="row">
            <div class="col"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: center;"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: right;"><img src="/storage/mask-group-33.png"></div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid blog">

        <div>
            <p class="blog-title">Our Blog</p>
        </div>

        <!-- Grid row -->

        <!-- Grid row -->
        <div id="gallery">
            <!-- Grid column -->
            <div class="pics animation all 1 custom-col-6">
                <div class="circle-date color_black">
                    <label><span class="font-size-16">28 </span><br>AUG</label>
                </div>
                <img class="img-fluid img-fluid-big" src="https://mdbootstrap.com/img/Photos/Vertical/mountain1.webp"
                     alt="Card image cap">
                <div class="centered">Same group Sets Real Estate World Record , Same group <br>
                    <button type="button" class="btn btn-read-more mt-2 read-more-big-project"> Read More</button>
                </div>
            </div>
            <!-- Grid column -->

            <div class="custom-col-6">
                <div>
                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record <br>
                            <button type="button" class="btn btn-read-more mt-2 read-more-small-project"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record<br>
                            <button type="button" class="btn btn-read-more read-more-small-project mt-2"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record <br>
                            <button type="button" class="btn btn-read-more read-more-small-project mt-2"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="pics animation all 2 custom-col-6">
                        <div class="circle-date-small color_black">
                            <label><span class="font-size-16">28 </span><br>AUG</label>
                        </div>
                        <img class="img-fluid img-fluid-small"
                             src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(35).webp"
                             alt="Card image cap">
                        <div class="centered centered-small">Same group Sets Real Estate World Record<br>
                            <button type="button" class="btn btn-read-more read-more-small-project mt-2"> Read More
                            </button>
                        </div>
                    </div>
                    <!-- Grid column -->
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Grid row -->
<section>
    <div class="container" style="max-width: 1435px;margin-left: 41px;margin-right: auto;">
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <h2>{{__('Frequently Searched')}} </h2>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <ul class="list-freq-search">
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Dubai">{{__('Properties for rent in')}}{{__('Dubai')}}</a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Abu Dhabi">{{__('Properties for rent in')}}{{__('Abu Dhabi')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Ras Al Khaimah">{{__('Properties for rent in')}}{{__('Ras Al Khaimah')}}  </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Ajman">{{__('Properties for rent in')}}{{__('Ajman')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Fujairah">{{__('Properties for rent in')}}{{__('Fujairah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Sharjah">{{__('Properties for rent in')}}{{__('Sharjah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=rent&location=Umm Al Quwain">{{__('Properties for rent in')}}{{__('Umm Al Quwain')}}  </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <ul class="list-freq-search">
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Dubai">{{__('Properties for sale in')}}{{__('Dubai')}}</a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Abu Dhabi">{{__('Properties for sale in')}}{{__('Abu Dhabi')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Ras Al Khaimah">{{__('Properties for sale in')}}{{__('Ras Al Khaimah')}}  </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Ajman">{{__('Properties for sale in')}}{{__('Ajman')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Fujairah">{{__('Properties for sale in')}}{{__('Fujairah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Sharjah">{{__('Properties for sale in')}}{{__('Sharjah')}} </a>
                            </li>
                            <li class="li-feq-list"><a
                                    href="properties?type=sale&location=Umm Al Quwain">{{__('Properties for sale in')}}{{__('Umm Al Quwain')}}  </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col" style="padding: 51px;">

                <img src="/storage/pexels-pixabay-280222.jpg"
                     style="border-radius: 155px;width: 100%;height: 324px;border: 20px solid white;box-shadow: 0px 0px 5px rgba(33,37,41,0.54);">
            </div>
        </div>
    </div>
</section>

<div class="col-sm-12">
    <nav class="d-flex justify-content-center pt-3" aria-label="Page navigation example">
        {!! $projects->withQueryString()->onEachSide(1)->links() !!}
    </nav>
</div>
<br>
<br>
