<div class="search-box">
    <div class="screen-darken"></div>
    <div class="search-box-content">
        <div class="d-md-none bg-primary p-2">
            <span class="text-white">{{ __('Filter') }}</span>
            <span class="float-right toggle-filter-offcanvas text-white">
                <i class="far fa-times-circle"></i>
            </span>
        </div>
        <div class="search-box-items">
            <div class="row ml-md-0 mr-md-0">
                <div class="col-lg-4 col-md-12 px-1">
                    <div class="form-group">
                        <label for="location" class="control-label" style="font-size: 20px;color: #034D8A;font-weight: bold;"><i class="fa fa-map-marker" style="padding-right: 5px;" aria-hidden="true"></i>{{__('Location')}}</label>
                        <div class="select--arrow">
                            <select id="citie" name="location" class="form-control filter-dropdown">
                                <option value="">{{ __('Select') }}</option>
                                    <option value="Dubai">{{__('Dubai')}}</option>
                                    <option value="Abu Dhabi">{{__('Abu Dhabi')}}</option>
                                    <option value="Sharjah">{{__('Sharjah')}}</option>
                                    <option value="Ajman">{{__('Ajman')}}</option>
                                    <option value="Umm Al Quwain">{{__('Umm Al Quwain')}}</option>
                                    <option value="Fujairah">{{__('Fujairah')}}</option>
                                    <option value="Ras Al Khaimah">{{__('Ras Al Khaimah')}}</option>

                            </select>
                            <i class="fas fa-angle-down"></i>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-12 px-1">
                    <div class="form-group">
                        {!! Theme::partial('real-estate.filters.categories', ['categories' => $categories]) !!}

                    </div>
                    {{-- {!! Theme::partial('real-estate.filters.choices', ['type' => $type, 'categories' => $categories, 'labelDefault' => __('Type, category...')]) !!} --}}
                </div>

                @if ($type == 'property')
                    <div class="col-lg-4 col-md-12 px-1">
                        <div class="form-group">
                        <label for="select-type" class="control-label" style="font-size: 20px;color: #034D8A;font-weight: bold;"><i class="fas fa-dollar-sign" style="padding-right: 5px;" aria-hidden="true"></i>{{ __('Price range') }}</label>
                        <div class="dropdown filter-dropdown2">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuPrice" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <span>{{ __('All prices') }}</span>
                            </button>
                            <div class="dropdown-menu" style="min-width: 20em;" aria-labelledby="dropdownMenuPrice">
                                <div class="dropdown-item">
                                    {!! Theme::partial('real-estate.filters.price') !!}
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>

                @else
                    <div class="col-lg-2 col-md-12 px-1">
                        {!! Theme::partial('real-estate.filters.floor') !!}
                    </div>
                    <div class="col-lg-2 col-md-12 px-1">
                        <label for="select-type" class="control-label">{{ __('Flat range') }}</label>
                        <div class="dropdown mb-2 filter-dropdown2">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuFlat" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <span>{{ __('All squares') }}</span>
                            </button>
                            <div class="dropdown-menu" style="min-width: 20em;" aria-labelledby="dropdownMenuFlat">
                                <div class="dropdown-item">
                                    {!! Theme::partial('real-estate.filters.flat') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="row ml-md-0 mr-md-0">
                <div class="col-lg-2 col-md-12 px-1">
                    {!! Theme::partial('real-estate.filters.bedroom') !!}
                </div>
                <div class="col-lg-2 col-md-12 px-1">
                    {!! Theme::partial('real-estate.filters.bathroom') !!}
                </div>
                @if ($type == 'property')
                <div class="col-lg-4 col-md-12 px-1">
                    <div class="form-group">
                        {{-- <label for="select-category" class="control-label">{{ __('Category') }}</label> --}}
                        <label for="select-features" class="control-label" style="font-size: 20px;color: #034D8A;font-weight: bold;"><i class="fas fa-hotel" style="padding-right: 5px;"></i>{{__('Property Features')}} </label>
                        <div class="select--arrow">
                            <select name="features" id="features" class="form-control filter-dropdown">
                                <option value="">{{ __('-- Select --') }}</option>
                                @foreach($features as $feature)
                                    <option value="{{ $feature->name }}" @if (request()->input('features') == $feature->name) selected @endif>{{ $feature->name }}</option>
                                @endforeach
                            </select>
                            <i class="fas fa-angle-down"></i>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-lg-4 col-md-12 px-1">
                    <label for="select-type" class="control-label" style="font-size: 20px;color: #034D8A;font-weight: bold;"><i class="fa fa-arrows-alt" style="padding-right: 5px;" aria-hidden="true"></i>{{ __('Square range') }}</label>

                        <div class="dropdown filter-dropdown2">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuSquare" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <span>{{ __('All squares') }}</span>
                            </button>
                            <div class="dropdown-menu" style="min-width: 20em;" aria-labelledby="dropdownMenuSquare">
                                <div class="dropdown-item">
                                    {!! Theme::partial('real-estate.filters.square') !!}
                                </div>
                            </div>
                        </div>
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <div class="col-lg-2 col-xl-1 col-md-2 px-1 button-search-wrapper" style="align-self: flex-end;">
                        <div class="btn-group text-center w-100 ">
                            <button type="submit" class="btn btn-primary btn-full">{{ __('Search') }}</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">{{__('Toggle Dropdown')}} </span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button class="dropdown-item" type="reset">{{ __('Reset filters') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
