<div class="item" data-lat="{{ $property->latitude }}" data-long="{{ $property->longitude }}">
    <div class="blii">
        <div class="img">
            
            <img class="thumb"
                data-src="{{ RvMedia::getImageUrl($property->image, 'small', false, RvMedia::getDefaultImage()) }}"
                src="{{ RvMedia::getImageUrl($property->image, 'small', false, RvMedia::getDefaultImage()) }}"
                alt="{{ $property->name }}">
        </div>
        <a href="{{ $property->url }}" class="linkdetail"></a>
        <div class="media-count-wrapper">
            <div class="media-count">
                <img src="{{ Theme::asset()->url('images/media-count.svg') }}" alt="media">
                <span>{{ count($property->images) }}</span>
            </div>
        </div>
        <div class="status">
            
            <a href="#" class="text-orange heart add-to-wishlist" data-id="{{ $property->id }}"
                title="{{ __('I care about this property!!!') }}"><i style="color: #fff;
                font-size: 30px;" class="far fa-heart"></i></a>
        </div>
        
        
    </div>

    <div class="description">
        <span style="width: 100%;
        text-align: left;
        font-size: 14px;
        display: block;
        font-weight: 400;">{{ $property->category->name }}, {{ $property->city->name }}</span>
        
        <a href="{{ $property->url }}">
            <h5 style="font-weight: 400;
            font-size: 26px !important;
            margin-bottom: 8px;
            margin-top: 0;">{{ $property->name }}</h5>
            {{-- <p class="dia_chi"><i class="fas fa-map-marker-alt"></i> {{ $property->city->name }},
                {{ $property->city->state->name }}</p> --}}
            <span style="width: 100%; text-align: left; display: block; color: #148ac8; font-weight: 600; font-size: 18px;">{{ format_price($property->price, $property->currency) }}</span>
        </a>
        <p style="font-size: 14px;color: #7d8285;">
            @if ($property->number_bedroom)
                <span data-original-title="{{ __('Number of rooms') }}">
                    {{-- <i><img src="{{ Theme::asset()->url('images/bed.svg') }}" alt="icon"></i>  --}}
                    {{ $property->number_bedroom }} {{ __('BDR') }} /
                </span>
            @endif
            @if ($property->number_bathroom)
                <span data-toggle="tooltip" data-placement="top"
                    data-original-title="{{ __('Number of rest rooms') }}">
                        {{-- <img src="{{ Theme::asset()->url('images/bath.svg') }}" alt="icon"></i> --}}
                            {{ $property->number_bathroom }} {{ __('BATH') }} /</span>
            @endif
            @if ($property->square)
                <span data-toggle="tooltip" data-placement="top" data-original-title="{{ __('Square') }}">
                    {{-- <i><img src="{{ Theme::asset()->url('images/area.svg') }}" alt="icon"></i>  --}}
                   {{ $property->square_text }}
                </span>
            @endif
            {!! apply_filters('property_item_listing_extra_info', null, $property) !!}
        </p>
        <div style="padding: 15px 0px 40px 0px;">
            <a href="{{ $property->url }}">
                <div class="share-icons"><i class="fas fa-envelope" style="color: #148ac8;font-size: 25px;"></i><i class="fa fa-phone" style="color: #148ac8;font-size: 25px;"></i><i class="fa fa-share-alt" style="color: #148ac8;font-size: 25px;"></i></div>
            </a>
            <div class="bottom-badge" style="background: #c3daea;"><span>{!! $property->status !!}</span></div>
        </div>
    </div>
</div>
