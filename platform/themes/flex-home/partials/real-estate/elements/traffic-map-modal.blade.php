<div id="zxcvbnm">
    <h5 class="headifhouse">{{ __('Location') }}</h5>
    
    <div class="traffic-map-container">
        <div class="row">
            <div class="col-4 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4" style="padding-top: 52px;">
                
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xxl-1"><i class="fa fa-map-marker" style="font-size: 57px;color: #148ac8;"></i></div>
                            <div class="col"><span style="color: #2c3949;font-size: 30px;font-weight: bold;">@if (!empty($location)) {{ $location }} @endif<br></span><span style="font-size: 26px;color: #2c3949;"></span></div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin:20px 0px;">
                    <div class="col">
                        {{-- <p><br>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages<br><br></p> --}}
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col"><button class="btn btn-primary" type="button" style="background: rgb(4,78,138);padding: 12px;border-radius: 0px;font-size: 14px;font-weight: 500;"><a href="tel:{{$phone ?? ''}}" class="btn btn-outline-link" type="button" style="color:#fff;">{{__('Contact an agent')}}</a></button></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8" style="padding: 51px;">
                <div id="trafficMap" class="w-100 h-100" style="border-radius: 155px;width: 100%;height: 324px;border: 20px solid white;box-shadow: 0px 0px 5px rgba(33,37,41,0.54);"></div>
                
            </div>
        </div>
        {{-- <div class="row justify-content-center">
            <div class="col-12">
                <div id="trafficMap" class="w-100 h-100"></div>
            </div>
        </div> --}}
    </div>
</div>