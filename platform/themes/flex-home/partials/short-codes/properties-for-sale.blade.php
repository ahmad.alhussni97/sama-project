<div class="padtop50">
    <div class="box_shadow custom-for-sale">
        <div class="container-fluid w90">
            <div class="homehouse projecthome">
                <div class="row">
                    <div class="col-12">
                        @if(BaseHelper::clean($title) == "Our Featured Properties")
                        <h2 style="text-align: center;">{!! BaseHelper::clean($title) !!}</h2>
                        @else
                        <h2 data-text="Sale">{!! BaseHelper::clean($title) !!}</h2>
                        @endif
                        @if ($subtitle)
                            <p>{!! BaseHelper::clean($subtitle) !!}</p>
                        @endif
                    </div>
                </div>
                <property-component type="sale" url="{{ route('public.ajax.properties') }}"></property-component>
            </div>
        </div>
    </div>
</div>
