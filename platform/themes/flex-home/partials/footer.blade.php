<!--FOOTER-->
<footer>
    <br>
    <div class="container-fluid w90">
        <div class="row">
            <div class="col-sm-3" style="text-align: center;">
                @if (theme_option('logo'))
                    <p>
                        <a href="{{ route('public.index') }}">
                            <img src="{{ RvMedia::getImageUrl(theme_option('logo'))  }}" style="max-height: 71px" alt="{{ theme_option('site_name') }}">
                        </a>
                    </p>
                    <div class="social-icons"><a href="#"><i class="fab fa-twitter"></i></a><a href="#"><i class="fab fa-facebook-f"></i></a><a href="#"><i class="fab fa-snapchat"></i></a><a href="#"><i class="fab fa-youtube"></i></a></div>
                @endif

            </div>
            <div class="col-sm-12 col-md-8 col-lg-7 padtop10">
                <div class="row">
                    {!! dynamic_sidebar('footer_sidebar') !!}
                </div>
            </div>
            <div class="col-3 col-sm-12 col-md-auto col-lg-2">
                <div class="row">
                    <div class="col" style="text-align: center;"><button class="btn btn-primary" type="button" style="background: rgb(4,78,138);width: 119px;padding: 12px;border-radius: 0px;font-size: 14px;font-weight: 500;">{{__('Login')}} </button></div>
                </div>
                <div class="row">
                    <div class="col" style="text-align: center;"><button class="btn btn-primary" type="button" style="background: rgba(4,78,138,0);width: 119px;padding: 12px;border-radius: 0px;font-weight: 500;font-size: 14px;margin-top: 20px;color: rgb(4,78,138);border-width: 3px;border-color: rgb(4,78,138);">{{__('Sell with us')}} </button></div>
                </div>
            </div>
        </div>
        <div class="row" style="display: none">
            <div class="col-12">
                {!! Theme::partial('language-switcher') !!}
            </div>
        </div>
        <div class="copyright">
            <div class="col-sm-12">
                <p class="text-center">
                    {!! BaseHelper::clean(theme_option('copyright')) !!}
                </p>
            </div>
        </div>
    </div>
</footer>
<!--FOOTER-->

<script>
    window.trans = {
        "Price": "{{ __('Price') }}",
        "Number of rooms": "{{ __('Number of rooms') }}",
        "Number of rest rooms": "{{ __('Number of rest rooms') }}",
        "Square": "{{ __('Square') }}",
        "No property found": "{{ __('No property found') }}",
        "million": "{{ __('million') }}",
        "billion": "{{ __('billion') }}",
        "in": "{{ __('in') }}",
        "Added to wishlist successfully!": "{{ __('Added to wishlist successfully!') }}",
        "Removed from wishlist successfully!": "{{ __('Removed from wishlist successfully!') }}",
        "I care about this property!!!": "{{ __('I care about this property!!!') }}",
    }
    window.themeUrl = '{{ Theme::asset()->url('') }}';
    window.siteUrl = '{{ url('') }}';
    window.currentLanguage = '{{ App::getLocale() }}';
</script>

<!--END FOOTER-->

<div class="action_footer">
    <a href="#" class="cd-top" @if (!Theme::get('hotlineNumber') && !theme_option('hotline')) style="top: -40px;" @endif><i class="fas fa-arrow-up"></i></a>
    @if (Theme::get('hotlineNumber') || theme_option('hotline'))
        <a href="tel:{{ Theme::get('hotlineNumber') ?: theme_option('hotline') }}" style="color: white;font-size: 17px;"><i class="fas fa-phone"></i> <span>  &nbsp;{{ Theme::get('hotlineNumber') ?: theme_option('hotline') }}</span></a>
    @endif
</div>

    {!! Theme::footer() !!}
</body>
</html>
